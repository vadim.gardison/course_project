var newsRender = (newsObject) => {
  // console.log('newsRender', newsObject.author);
  // console.log('newsRender', newsObject.text);
  // console.log('newsRender', newsObject.imgUrl);
  
  // Feed body
  var date = new Date();
  var newsFeedDiv = document.getElementById('newsFeedDiv');
  var feedContainerDiv = document.getElementById('feedContainer');
  var itemNewsFeed = document.createElement('div');
  itemNewsFeed.className = 'row';
  var itemBody = document.createElement('div');
  itemBody.className = 'col';
  var authorTitleEl = document.createElement('h5');
  authorTitleEl.classList.add('author-title-el')
  authorTitleEl.innerText = newsObject.author;
  var timeEl = document.createElement('h6');
  timeEl.innerText = date.toLocaleDateString() + ' ' + date.toLocaleTimeString({ hour12: false });
  var feedBody = document.createElement('p');
  feedBody.classList.add('feed-paragraph');
  feedBody.innerText = newsObject.text;
  
  var feedImg = new Image();
  feedImg.id = 'feedImgImg' + newsObject.id;
  feedImg.className = 'img-thumbnail';
  feedImg.src = newsObject.imgUrl;

  var w100Div = document.createElement('div');
  w100Div.className = 'w-100';
  
  // Comment header (likes, show & write buttons)
  var commentBody = document.createElement('div');
  commentBody.className = 'col';
  var commentBodyHeaderRow = document.createElement('div');
  commentBodyHeaderRow.classList.add('row', 'comment-body-header-row');
  commentBodyHeaderRow.id = 'commentBodyHeaderRowId' + newsObject.id;
  var commentBodyHeaderRowLikes = document.createElement('div');
  commentBodyHeaderRowLikes.innerText = 'likes: '
  commentBodyHeaderRowLikes.classList.add('col', 'comment-body-header-row-likes');
  var likesNumber = document.createElement('span');
  likesNumber.id = 'likesNumber_' + newsObject.id;
  likesNumber.innerText = newsObject.likes;
  commentBodyHeaderRowLikes.appendChild(likesNumber);
  var putLike = document.createElement('span');
  var likeImg = new Image();
  likeImg.classList.add('like-img');
  likeImg.src = '../img/like.png';
  putLike.appendChild(likeImg);
  putLike.addEventListener('click', (e) => {
    newsObject.likes++;
    likesNumber.innerText = '';
    likesNumber.innerText = newsObject.likes;
  });
  commentBodyHeaderRowLikes.appendChild(putLike);
  var commentsCounter = 0;
  var commentBodyHeaderRowShowComments = document.createElement('div');
  commentBodyHeaderRowShowComments.classList.add('comments-control');
  commentBodyHeaderRowShowComments.innerText = `Show Comments(${commentsCounter})`;
  commentBodyHeaderRowShowComments.classList.add('col-12', 'col-md-auto');
  commentBodyHeaderRowShowComments.addEventListener('click', (e) => {
    showCommentsBodyCol.classList.toggle('hideEl');
    writeCommentsBodyCol.classList.add('hideEl');
    commentBodyHeaderRowShowComments.innerText = commentBodyHeaderRowShowComments.innerText == `Show Comments(${commentsCounter})` ? `Hide Comments(${commentsCounter})` : `Show Comments(${commentsCounter})`;
  })
  var commentBodyHeaderRowWriteComments = document.createElement('div');
  commentBodyHeaderRowWriteComments.classList.add('comments-control');
  commentBodyHeaderRowWriteComments.innerText = 'Write Comment';
  commentBodyHeaderRowWriteComments.classList.add('col-12', 'col-md-auto');
  commentBodyHeaderRowWriteComments.addEventListener('click', (e) => {
    writeCommentsBodyCol.classList.toggle('hideEl');
    showCommentsBodyCol.classList.add('hideEl');
  })
  
  // Show comments body
  var showCommentsBody = document.createElement('div');
  showCommentsBody.id = 'showCommentsBodyId' + newsObject.id;
  showCommentsBody.className = 'row';
  var showCommentsBodyCol = document.createElement('div');
  showCommentsBodyCol.classList.add('col', 'hideEl');
  var noComments = document.createElement('div');
  noComments.innerText = 'No comments';
  showCommentsBodyCol.appendChild(noComments);
  showCommentsBody.appendChild(showCommentsBodyCol);

  // Write comment body
  var writeCommentsBody = document.createElement('div');
  writeCommentsBody.id = 'writeCommentsBodyId' + newsObject.id;
  writeCommentsBody.className = 'row';
  var writeCommentsBodyCol = document.createElement('div');
  writeCommentsBodyCol.classList.add('col', 'hideEl');
  // writeCommentsBodyCol.innerText = 'Write comments';
  var addCommentForm = document.createElement('form');
  addCommentForm.id = 'addCommentFormId' + newsObject.id;
  var commentAuthorDiv = document.createElement('div');
  commentAuthorDiv.classList.add('form-group');
  var commentAuthorInputLabel = document.createElement('label');
  commentAuthorInputLabel.innerText ='Author';
  var commentAuthorInput = document.createElement('input');
  commentAuthorInput.classList.add('form-control');
  commentAuthorInput.id = 'inputCommentAuthor' + newsObject.id;
  commentAuthorInput.placeholder = 'Enter your name';
  var commentTextDiv = document.createElement('div');
  commentTextDiv.classList.add('form-group');
  var commentTextAreaLabel = document.createElement('label');
  commentTextAreaLabel.innerText = 'Comment';
  var commentTextArea = document.createElement('textarea');
  commentTextArea.classList.add('form-control');
  commentTextArea.id = 'inputCommentText' + newsObject.id;
  commentTextArea.placeholder = 'Enter comment\'s text';
  var addCommentButton = document.createElement('button');
  addCommentButton.id = 'addCommentButtonId' + newsObject.id;
  addCommentButton.type = 'submit';
  addCommentButton.classList.add('btn', 'btn-primary');
  addCommentButton.innerText = 'Add comment';
  var autoRenderId = 0;
  addCommentButton.addEventListener('click', (e) => {
    e.preventDefault();
    autoRenderId++;
    commentsCounter++;
    noComments.classList.add('hideEl');
    commentBodyHeaderRowShowComments.innerText = `Show Comments(${commentsCounter})`;
    if (commentTextArea.value.length > 0) {
      var commentAuthorRenderDiv = document.createElement('div');
      commentAuthorRenderDiv.classList.add('col', 'comment-div');
      commentAuthorRenderDiv.id = autoRenderId;
      var commentAuthorRender = document.createElement('h6');
      // showCommentsBodyCol.innerText = '';
      commentAuthorRender.innerText = commentAuthorInput.value;
      var commentTimeRender = document.createElement('h6');
      var commentDate = new Date();
      commentTimeRender.innerText = commentDate.toLocaleDateString() + ' ' + commentDate.toLocaleTimeString({ hour12: false });
      var commentTextRender = document.createElement('p');
      commentTextRender.innerText = commentTextArea.value;
      commentAuthorRenderDiv.appendChild(commentAuthorRender);
      commentAuthorRenderDiv.appendChild(commentTimeRender);
      commentAuthorRenderDiv.appendChild(commentTextRender);
      showCommentsBodyCol.appendChild(commentAuthorRenderDiv);
      showCommentsBodyCol.classList.toggle('hideEl');
      writeCommentsBodyCol.classList.add('hideEl');
      commentBodyHeaderRowShowComments.innerText = `Hide Comments(${commentsCounter})`;
      commentAuthorInput.value = '';
      commentTextArea.value = '';
    }
  })
  commentAuthorDiv.appendChild(commentAuthorInputLabel);
  commentAuthorDiv.appendChild(commentAuthorInput);
  addCommentForm.appendChild(commentAuthorDiv);
  commentTextDiv.appendChild(commentTextAreaLabel);
  commentTextDiv.appendChild(commentTextArea);
  addCommentForm.appendChild(commentTextDiv);
  addCommentForm.appendChild(addCommentButton);
  writeCommentsBodyCol.appendChild(addCommentForm);
  writeCommentsBody.appendChild(writeCommentsBodyCol);
  itemNewsFeed.appendChild(itemBody);
  itemBody.appendChild(authorTitleEl);
  itemBody.appendChild(timeEl);
  itemBody.appendChild(feedBody);
  itemBody.appendChild(feedImg);
  itemNewsFeed.appendChild(w100Div);
  itemNewsFeed.appendChild(commentBody);
  commentBody.appendChild(commentBodyHeaderRow);
  commentBody.appendChild(showCommentsBody);
  commentBody.appendChild(writeCommentsBody);
  commentBodyHeaderRow.appendChild(commentBodyHeaderRowLikes);
  commentBodyHeaderRow.appendChild(commentBodyHeaderRowShowComments);
  commentBodyHeaderRow.appendChild(commentBodyHeaderRowWriteComments);
  var horizontalLine = document.createElement('hr');
  horizontalLine.classList.add('hor-line');
  feedContainerDiv.insertBefore(horizontalLine, feedContainerDiv.firstChild);
  feedContainerDiv.insertBefore(itemNewsFeed, feedContainerDiv.firstChild);
}