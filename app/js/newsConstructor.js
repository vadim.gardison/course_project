var publishButton = document.getElementById('publish');
var inputAuthor = document.getElementById('inputAuthor');
var inputNewsText = document.getElementById('inputNewsText');
var inputImageFile = document.getElementById('inputImageFile');
var previewImage = document.getElementById('previewImg');
var previewDiv = document.getElementById('previewDiv');
var pError = document.createElement('p');
pError.setAttribute('style', 'display: none');
previewDiv.appendChild(pError);
var newsObjectId = 0;

var createPreviewImg = () => {
  var newImg = new Image();
  newImg.id = 'previewImg';
  newImg.className = 'img-thumbnail';
  newImg.src = inputImageFile.value;
  pError.setAttribute('style', 'display: none');
  previewDiv.appendChild(newImg);
}

var renderImage = (img) => {
  img.id = 'previewImg';
  img.className = 'img-thumbnail';
  pError.setAttribute('style', 'display: none');
  previewDiv.appendChild(img);
}

var createErrorMsg = () => {
  pError.setAttribute('style', 'display:block');
  pError.innerText = 'Enter valid image URL';
  pError.className = 'error';
}

function loadImagePromise( url ){
    return new Promise( (resolve, reject) => {
      let imageElement = new Image();
      imageElement.src = url;
          imageElement.onload = function(){
            resolve( imageElement );
          };
          imageElement.onerror = function(){
            reject(
            );
          };
    });
  }

inputImageFile.addEventListener('input', (e) => {
    if (document.getElementById('previewImg')) {
      document.getElementById('previewImg').remove();
      loadImagePromise(inputImageFile.value);
    } else {
      loadImagePromise(inputImageFile.value).then(
        res => {
          renderImage(res);
        },
        err => {
          createErrorMsg();
          throw new Error('Rejected Custom: ' + err);
        }
      );
    }
})

publishButton.addEventListener('click', (event) => {
  event.preventDefault();
  newsObjectId++;
  inputAuthor.value = inputAuthor.value.trim();
  inputNewsText.value = inputNewsText.value.trim();
  inputImageFile.value = inputImageFile.value.trim();
  if (inputAuthor.value.length > 0 && inputNewsText.value.length > 0 && inputImageFile.value.length > 0 && document.getElementById('previewImg')) {
    console.log(new newsObject(newsObjectId, inputAuthor.value, inputNewsText.value, inputImageFile.value));
    newsRender(new newsObject(newsObjectId, inputAuthor.value, inputNewsText.value, inputImageFile.value));
    inputAuthor.value = '';
    inputNewsText.value = '';
    inputImageFile.value = '';
    document.getElementById('previewImg').remove();
  } else {
    alert('Заповніть усі поля форми !!!')
  }
})